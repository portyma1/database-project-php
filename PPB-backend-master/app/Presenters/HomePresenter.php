<?php
declare(strict_types=1);
namespace App\Presenters;
use Nette;
final class HomePresenter extends Nette\Application\UI\Presenter
{

	public function startup(): void
	{
		parent::startup();
	
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}
	}
	

    public function __construct(
		private Nette\Database\Explorer $database,
	) {
	}
	public function renderDefault(): void {
		$uniqueProducts = $this->database
			->table('produkt')
			->select('produktid')
			->group('produktid')
			->count();
		
		$this->template->uniqueProductsCount = $uniqueProducts;
	}
	
}