<?php
namespace App\Presenters;

use Nette;

final class ProductPresenter extends Nette\Application\UI\Presenter
{
    public function startup(): void
    {
        parent::startup();
    
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }
    
    public function __construct(
        private Nette\Database\Explorer $database,
    ){}

    public function renderDefault(): void {$this->template->products = $this->database
        ->table('produkt');}

    public function renderShow(int $productId): void
    {
 
    $product = $this->database->table("produkt")->get($productId);
    if (!$product)
    {
        $this->error("Produkt nenalezen");
    }

    $this->template->product=$product;

    }

}