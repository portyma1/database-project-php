<?php 
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class SupplierPresenter extends Nette\Application\UI\Presenter
{
    private Nette\Database\Explorer $database;

    public function __construct(Nette\Database\Explorer $database)
    {
        $this->database = $database;
    }
    public function startup(): void
    {
        parent::startup();
    
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }
    

    public function renderEdit(int $supplierId): void
    {
        $supplier = $this->database
            ->table('dodavatele')
            ->get($supplierId);
        if (!$supplier) {
            $this->error('Dodavatel nenalezen');
        }
        $this->getComponent('supplierForm')
            ->setDefaults($supplier->toArray());
    }
    

    public function renderDefault()
    {
        $suppliers = $this->database->table('dodavatele');
        $this->template->suppliers = $suppliers;
    }

    public function renderShow(int $supplierId)
    {
        $supplier = $this->database->table("dodavatele")->get($supplierId);

        if(!$supplier) {
            $this->error("Dodavatel nenalezen");
        }

        $this->template->supplier = $supplier;
    }
    
 
    
    
    public function supplierFormSucceeded(array $values)
    {


         $supplierId = $this->getParameter('supplierId');
         
         if($supplierId) {
             $supplier = $this->database->table('dodavatele')->get($supplierId);
             $supplier->update($values);
         } else {
             $this->database->table('dodavatele')->insert($values);
         }
         
         
         /*
        $supplier = $this->database->table('dodavatele')->get($supplierId);
    
        if (!$supplier) {
            $this->error('Dodavatel nebyl nalezen.');
        }
    
        $supplier->update([
            'Nazevfirmy' => $values['Nazevfirmy'],
            'Cislouctu' => $values['Cislouctu'],
            'Telefon' => $values['Telefon'],
        ]); */
    
        $this->flashMessage('Dodavatel byl uložen.', 'success');
        $this->redirect('default', ['supplierId' => $supplierId]);
    }
    

   

public function createComponentSupplierForm(): Form
{
    $form = new Form;
        
    $form->addText('Nazevfirmy', 'Název')
        ->setRequired();
    $form->addText('Cislouctu', 'BÚ');
    $form->addText('Telefon', 'Telefon');
        
    $form->addSubmit('submit', 'Uložit');
        
    $form->onSuccess[] = [$this, 'supplierFormSucceeded'];
        
    $supplierId = $this->getParameter('supplierId', null);
        
    if ($supplierId) {
        // úprava existujícího dodavatele
        $supplier = $this->database->table('dodavatele')->get($supplierId);
            
        if (!$supplier) {
            $this->error('Dodavatel nebyl nalezen.');
        }
    
        $form->setDefaults([
            'Nazevfirmy' => $supplier->Nazevfirmy,
            'Cislouctu' => $supplier->Cislouctu,
            'Telefon' => $supplier->Telefon,
        ]);
    } else {
        // vytvoření nového dodavatele
        $form->addSubmit('create', 'Vytvořit nového dodavatele');
                
        $form->onSuccess[] = function(Form $form, array $values) {
            $this->database->table('dodavatele')->insert($values);
                    
            $this->flashMessage('Nový dodavatel byl úspěšně vytvořen.', 'success');
            $this->redirect('default');
        };
    }
            
    return $form;
}

 


}
?>