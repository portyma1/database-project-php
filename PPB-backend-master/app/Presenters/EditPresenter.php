<?php
namespace App\Presenters;
 
use Nette;
use Nette\Application\UI\Form;

final class EditPresenter extends Nette\Application\UI\Presenter
{
	public function __construct(
		private Nette\Database\Explorer $database,
	) {        
	}


    public function startup(): void
    {
        parent::startup();
    
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function renderEdit(int $productId): void
    {
        $product = $this->database
            ->table('produkt')
            ->get($productId);
        if (!$product) {
            $this->error('Produkt nenalezen');
        }
        $this->getComponent('productForm')
            ->setDefaults($product->toArray());
    }

    protected function createComponentProductForm(): Form
    {
        $form = new Form;
        $form->addText("Nazev", "Název produktu")->setRequired();
        $form->addInteger("Cena","Cena produktu");
        $form->addTextArea("Popis", "Popis produktu");
        $categories = $this->database->table('kategorie')->fetchPairs('KategorieID', 'Nazevkategorie');
        $form->addSelect('KategorieID', 'Kategorie:', $categories);
        $form->addUpload('Obrazek', 'Obrázek:')
	->addRule($form::Image, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.')  ;   
        $form->addSubmit("send","Publikovat");
        $form->onSuccess[] = [$this, "productFormSucceeded"];
        return $form;
    }

    public function productFormSucceeded(array $data): void
{
    $productId = $this->getParameter("productId");
    $file = $data["Obrazek"];

    if ($file->isOk() && $file->isImage()) {
        $file_ext = strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
        $file_name = uniqid(rand(0,20), TRUE).$file_ext;
        $file->move("../../product_pics/".$file_name);
        $file_url = $file_name;
    } else {
        $file_url = null;
    }

    if ($file_url) {
        $data["Obrazek"] = $file_url;
    } else {
        unset($data["Obrazek"]);
    }

    if ($productId === null) { // produkt neexistuje v databázi
        $stock = $this->database->table('Sklad')->insert([]);
        $stockId = $stock->SkladID;
        $data['SkladID'] = $stockId;
        $result = $this->database->table('Produkt')->insert($data);
        $productId = $result->getPrimary(); // získání primárního klíče nového produktu
    } else { // produkt již existuje v databázi
        $result = $this->database->table('Produkt')->get($productId)->update($data);
    }

    $this->flashMessage('Produkt byl úspěšně publikován.', 'success');
    $this->redirect('Product:default');
}


}
