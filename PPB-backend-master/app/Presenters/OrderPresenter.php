<?php
namespace App\Presenters;

use Nette;

final class OrderPresenter extends Nette\Application\UI\Presenter
{
    public function startup(): void
    {
        parent::startup();
    
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }
    
    public function __construct(
        private Nette\Database\Explorer $database,
    ){}

    public function renderDefault(): void {$this->template->orders = $this->database
        ->table('objednavka');}

        public function renderShow(int $orderId): void
        {
            $order = $this->database->table('objednavka')->get($orderId);
            if (!$order) {
                $this->error('Objednávka nenalezena');
            }
            
            $items = $order->related('objednavka_detail.ObjednavkaID')->fetchAll();
        
            $this->template->order = $order;
            $this->template->items = $items;
        }
        

}