<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->appTitle = 'My App'; // Nastavte název vaší aplikace
    }

    public function createComponentFlashMessages()
    {
        $control = new \stdClass;
        $control->flash = $this->template->flashes;
        return $control;
    }
}
