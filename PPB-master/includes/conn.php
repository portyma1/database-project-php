<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "nette_shop"; 
// Constants
define("IMG_DIR", "product_pics/");
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
  die("Připojení do databáze selhalo: " . $conn->connect_error);
}
// Fetch function
function fetch($sql, $conn) {
  $fetched = $conn->query($sql);
  return $fetched;
}
?>