<?php 
// check if the cart array exists in the session, if not create it
if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}
// check if a product was added to the cart
if (isset($_GET['produkt'])) {
    // add the product id to the cart array
    $produkt = $_GET['produkt'];
    array_push($_SESSION['cart'], $produkt);
}
// prints an array 
print_r($_SESSION['cart']);
?>