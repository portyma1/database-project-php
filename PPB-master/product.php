<?php session_start(); ?>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Detail produktu</title>
<link rel="stylesheet" href="assets/style.css">
</head>
<body>
          <div class="wrapper">
<?php 
require 'includes/includes.php';
// If chosen product set in the URL...
if(isset($_GET["product"]))
{ $product_id = $_GET['product'];
  $sql = "SELECT nazevkategorie, produktid, CONCAT(FORMAT(cena, 2, 'cs_CZ'), ' Kč') cena_f, nazev, popis, obrazek, mnozstvi FROM produkt p INNER JOIN sklad s ON p.SkladID = s.SkladID  INNER JOIN kategorie k on p.KategorieID = k.KategorieID WHERE produktid =  $product_id";
  // Result
  //$result = $conn->query($sql2);
  $result = fetch($sql, $conn);
  // TODO ESHOP CART
  // Fetch from query
  if(!empty($result) && $result->num_rows > 0) {
    $row = $result->fetch_assoc();
        ?> 
            <div class="detail">
              <div class="info">
                <div class="category"><?php echo $row["nazevkategorie"];?></div>
                <div class="detail-name"><?php echo $row["nazev"]; ?></div>
                <div class="detail-description"><?php echo $row["popis"];?></div>
                <div class="detail-stock">Skladem: <?php echo $row["mnozstvi"]; ?> ks</div>
                <div class="detail-price"><?php echo $row["cena_f"]; ?></div>
                <div class="btn-wrapper">
                  <a href="cart/add.php?product=<?php echo $row["produktid"] ?>" class="btn1">Do košíku</a>
                </div>
              </div>
              <img src="<?php echo IMG_DIR . $row["obrazek"]; ?>" alt="Obrázek produktu"> 
            </div>
        <?php
  }
}
else 
  {
    echo "Chyba: požadovaný produkt nenalezen";
  }
$conn->close();
?>    </div>
  </body>
</html>