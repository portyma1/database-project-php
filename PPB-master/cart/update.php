<?php
session_start();
if (isset($_POST['product_id']) && isset($_POST['quantity'])) {
    $product_id = (int)$_POST['product_id'];
    $new_quantity = (int)$_POST['quantity'];

    $old_quantity = 0;
    foreach ($_SESSION['cart'] as $index => $id) {
        if ($id == $product_id) {
            $old_quantity++;
            unset($_SESSION['cart'][$index]);
        }
    }
    if ($new_quantity > 0) {
        for ($i = 0; $i < $new_quantity; $i++) {
            $_SESSION['cart'][] = $product_id;
        }
    }
    $_SESSION['cart'] = array_values($_SESSION['cart']);
}
header("Location: ../cart.php");
exit;
?>
