<?php
// start the session
session_start();
// check if the cart array exists in the session, if not create it
if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}
// check if a product was added to the cart
if (isset($_GET['product'])) {
    // add the product id to the cart array
    $product = $_GET['product'];
    array_push($_SESSION['cart'], $product);
    // Print cart array echo $_GET['product'];
}
// redirect back to the product detail page
header('Location: ../product.php?product=' . $_GET['product']);
exit();
?>
