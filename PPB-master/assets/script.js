function updateQuantity(elem, change) {
    let quantityInput = elem.parentNode.querySelector('input[name="quantity"]');
    let newQuantity = parseInt(quantityInput.value) + change;
    if (newQuantity >= 0) {
        quantityInput.value = newQuantity;
        elem.parentNode.querySelector('form').submit();
    }
}

function submitFormOnChange(elem) {
    elem.parentNode.querySelector('form').submit();
}
