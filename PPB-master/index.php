<?php session_start(); ?>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Domů</title>
<link rel="stylesheet" href="assets/style.css">
</head>
<body>
  <div class="wrapper">
<?php 
include 'includes/includes.php';
// Dotaz do DB na vsechny produkty
$sql1 = "SELECT nazevkategorie, produktid, CONCAT(FORMAT(cena, 2, 'cs_CZ'), ' Kč') cena_f, nazev, popis, obrazek, mnozstvi FROM produkt p INNER JOIN sklad s on p.SkladID = s.SkladID INNER JOIN kategorie k ON p.KategorieID = k.KategorieID";
$result = $conn->query($sql1);
// Vypis vsech produktu
echo '<div class="products-grid">';
if(!empty($result) && $result->num_rows > 0) {
  while($row = $result->fetch_assoc())
  { 
?>
 
<div class="product-card"> <div class="category"><?php echo $row["nazevkategorie"];?></div>
  <div class="title"><?php echo $row["nazev"];?></div>
 
    <img src="<?php echo IMG_DIR . $row["obrazek"]; ?>" alt="Obrázek produktu">
    <!-- <div class="card-description"><?php //echo $row["popis"]; ?></div> -->
    <div class="card-price"><?php echo $row["cena_f"]; ?></div>
    <a href="./product.php?product=<?php echo $row["produktid"];?>" class="btn1">Detail produktu</a>
  </div> 
   <?php
  }
}
  else { echo "Chyba: v databázi nejsou produkty";}
echo '</div>';
$conn->close();
?> </div>
  </body>
  </html>