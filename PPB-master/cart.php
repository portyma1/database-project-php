<?php session_start(); ?>
<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Košík</title>
        <link rel="stylesheet" href="assets/style.css">
    </head>
    <body>
        <div class="wrapper">
<?php 
require 'includes/includes.php';
// Total cart price
$total_price = 0;
// Count the number of each product in the cart
$counts = array();
foreach ($_SESSION['cart'] as $product_id) {
    if (isset($counts[$product_id])) {
        $counts[$product_id]++;       
    } else {
        $counts[$product_id] = 1;
    }
}
// Query the database for the details of products in cart
if (!empty($_SESSION['cart'])) {
    $product_ids = implode(',', $_SESSION['cart']);
    $sql = "
        SELECT produktid, cena, CONCAT(FORMAT(cena, 2, 'cs_CZ'), ' Kč') cena_f, nazev, popis, obrazek, mnozstvi
        FROM produkt p
        INNER JOIN sklad s ON p.SkladID = s.SkladID
        WHERE produktid IN ($product_ids)
    ";
    $result = fetch($sql, $conn); 
    // Display the cart contents with counts
    echo '<div class="cart-wrapper">';
    if (!empty($result) && $result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $count = isset($counts[$row['produktid']]) ? $counts[$row['produktid']] : 0;
            $total_price += $row['cena'] * $count;
            ?>
             <div class="cart-item content">
            <img src="<?php echo IMG_DIR . $row["obrazek"]; ?>" alt="Obrázek produktu">
            <div class="name"><?php echo $row["nazev"];?></div>
            <div class="price"><?php echo $row["cena_f"]; ?></div>
            <div class="quantity">
                <form method="POST" action="cart/update.php">
                    <input type="hidden" name="product_id" value="<?php echo $row['produktid']; ?>">
                    <input type="number" name="quantity" value="<?php echo $count; ?>" min="0">
                    <input type="submit" value="Změnit množství">
                </form>
            </div>
        </div>
            <?php
        }?>
        <div class="total content">    
            <div>Součet: <?php echo number_format($total_price, 2, ',', ' '); ?> Kč</div> 
    </div>
<div class="content">
            <form id="order-form" action="order/create.php" method="POST">
      <label for="jmeno">Jméno:</label>
      <input type="text" id="jmeno" name="jmeno"><br>
      <label for="prijmeni">Příjmení:</label>
      <input type="text" id="prijmeni" name="prijmeni"><br>
      <label for="email">E-mail:</label>
      <input type="email" id="email" name="email"><br>
      <label for="telefon">Telefon:</label>
      <input type="tel" id="telefon" name="telefon"><br>
      <label for="psc">PSČ:</label>
      <input type="number" id="psc" name="psc"><br>
      <label for="cp">Čp.:</label>
      <input type="text"id="cp" name="cp"><br>
       
 <label for="obec">Město:</label>
      <input type="text" id="obec" name="obec"><br>
      <label for="ulice">Ulice:</label>
      <input type="text" id="ulice" name="ulice"><br>
      
     
    <label for="doprava">Způsob dopravy</label>
    <select name="doprava">
    <?php
    $shipping_query = "SELECT typdopravyid AS id, nazev, cena FROM `typdopravy`;";
    $shipping_result = $conn->query($shipping_query);
    if (!empty($shipping_result) && $shipping_result->num_rows > 0) {
        while ($row2 = $shipping_result->fetch_assoc()) {
            $id = $row2['id'];
            $nazev = $row2['nazev'];
            $cena = $row2['cena'];
            echo '<option value="' . $id . '">' . $nazev . ' - ' . $cena . ' Kč</option>';
        }
    }
    ?>
</select>
<label for="platba">Způsob platby:</label>
<select name="platba" id="platba">
    <?php
    $payment_query = "SELECT typplatbyid AS id, nazev FROM `typplatby`;";
    $payment_result = $conn->query($payment_query);
    if (!empty($payment_result) && $payment_result->num_rows > 0) {
        while ($row = $payment_result->fetch_assoc()) {
            $id = $row['id'];
            $nazev_platby = $row['nazev'];
            echo '<option value="' . $id . '">' . $nazev_platby .'</option>';
        }
    }
    ?>
</select>

    <input class="btn1" type="submit" value="Objednat"> 
    </form>
</div>
<?php
    } else {echo "Chyba: žádné produkty nebyly nalezeny.";}
    echo '</div>';
} else {?><p class="content">Váš košík je prázdný.</p><?php
}
$conn->close();
?> 
</div>
</div>
</body>
</html>